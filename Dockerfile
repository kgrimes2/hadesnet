FROM golang:1.12.9-stretch AS builder
LABEL maintainer="kevingrimes2@me.com"

COPY . ./
RUN go build -o /hadesnet .

FROM scratch
COPY --from=builder /hadesnet .
ENTRYPOINT ["./hadesnet"]
